package Library;

import java.io.Serializable;

/**
 * Created by maryb on 02.06.2017.
 */
public class Bookcase implements Serializable{

    private Shell [] shelfs = new Shell[10];

    public void putShelf(int indexShell, Shell shelf){
        if (indexShell > 0 && indexShell < shelfs.length){
            shelfs[indexShell] = shelf;
        }
    }

    public Shell getShelf(int indexShelf) {
        if (indexShelf >= 0 && indexShelf < shelfs.length) {
            //System.out.println("Oto Twoja półka: ");
            return shelfs[indexShelf];
        }
        else{
            System.out.println("Nie ma półek");
            return null;
        }
    }
    public int getLengthBookcase(){
        return shelfs.length;
    }
}
