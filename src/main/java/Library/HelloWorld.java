package Library;

import java.util.Scanner;

/**
 * Created by maryb on 01.06.2017.
 */
public class HelloWorld {

    public static void main(String[] args) {


        Scanner line = new Scanner(System.in);

        // Bookcase [] bookcases = new Bookcase[10];

        Library library = new Library();

        boolean run = true;

        while (run) {
            System.out.println("Podaj komende i naciśnij enter: ");
            String command = line.nextLine();

            switch (command) {

                case "add": {

                    System.out.println("Indeks");

                    int index = line.nextInt();
                    line.skip("\n");   // omija entera

                    int indexShelfs = line.nextInt();
                    line.skip("\n");

                    int indexBookcase = line.nextInt();
                    line.skip("\n");


                    Book newBook = new Book();
                    System.out.println("Tytul");
                    newBook.setTitle(line.nextLine());
                    System.out.println("Autor");
                    newBook.setAuthor(line.nextLine());
                    System.out.println("Rok");
                    newBook.setYear(line.nextInt());

                    if (library.bookcases[indexBookcase] == null) {
                        library.bookcases[indexBookcase] = new Bookcase();
                    }

                    if (library.bookcases[indexBookcase].getShelf(indexShelfs) == null) {

                        Shell newShelf = new Shell();
                        library.bookcases[indexBookcase].putShelf(indexShelfs, newShelf);
                        // shelfs[indexShelfs] = new Shell();
                    }
                    library.bookcases[indexBookcase].getShelf(indexShelfs).putBook(index, newBook);
                    //bookcase.putShelf(indexShelfs, shelf);
                    break;
                }

                case "print": {
                    int index = line.nextInt();
                    line.skip("\n");
                    int indexShelfs = line.nextInt();
                    line.skip("\n");
                    int indexBookcase = line.nextInt();
                    line.skip("\n");

                    if (library.bookcases[indexBookcase] != null
                            && library.bookcases[indexBookcase].getShelf(indexShelfs) != null
                            && library.bookcases[indexBookcase].getShelf(indexShelfs).getBook(index) != null) {
                        System.out.println("Title: " + library.bookcases[indexBookcase].getShelf(indexShelfs).getBook(index).getTitle());
                        System.out.println("Author: " + library.bookcases[indexBookcase].getShelf(indexShelfs).getBook(index).getTitle());
                        System.out.println("Year: " + library.bookcases[indexBookcase].getShelf(indexShelfs).getBook(index).getTitle());
                    }
                    //bookcases[indexBookcase].getShelf(indexShelfs).getBook(index).printToOutput();
                    library.bookcases[indexBookcase].getShelf(index);
                    break;
                }

                case "print_all": {

                    for (int t = 0; t < library.bookcases.length; t++) {
                        if (library.bookcases[t] != null) {

                            for (int k = 0; k < library.bookcases[t].getLengthBookcase(); k++) {

                                if (library.bookcases[t].getShelf(k) != null) {
                                    for (int i = 0; i < library.bookcases[t].getShelf(k).getLengthShell(); i++) {  // przegladanie polki

                                        if (library.bookcases[t].getShelf(k).getBook(i) != null) {
                                            library.bookcases[t].getShelf(k).getBook(i).printToOutput();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                }

                case "quit":
                {
                    run = false;
                    break;
                }
                default:
                    System.out.println("Wrong!");
            }
        }

        library.writeAllBooksToFile();
        //library.writeToBinary("library");
    }
}
