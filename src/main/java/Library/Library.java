package Library;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by maryb on 03.06.2017.
 */
public class Library implements Serializable{

    Bookcase [] bookcases = new Bookcase[10];


    public void putBookcase (int indexBookcase, Bookcase bookcase)    {
        if (indexBookcase > 0 && indexBookcase < bookcases.length){
            bookcases[indexBookcase] = bookcase;
        }

    }

    public Bookcase getBookcase(int indexBookcase) {
        if (indexBookcase > 0 && indexBookcase < bookcases.length) {
            return bookcases[indexBookcase];
        }
        else{
            return null;
        }
    }

    public void writeAllBooksToFile(){
        try (FileWriter fw = new FileWriter("library.txt");   // tworzenie obiektu fw
             BufferedWriter bw = new BufferedWriter(fw);) {
            int count = 0;


            for (int t = 0; t < bookcases.length; t++) {
                if (bookcases[t] != null) {

                    for (int k = 0; k < bookcases[t].getLengthBookcase(); k++) {

                        if (bookcases[t].getShelf(k) != null) {
                            for (int i = 0; i < bookcases[t].getShelf(k).getLengthShell(); i++) {  // przegladanie polki

                                if (bookcases[t].getShelf(k).getBook(i) != null) {
                                    count++;
                                }
                            }
                        }
                    }
                }
            }

            bw.write(count + "");
            bw.newLine();


            for (int t = 0; t < bookcases.length; t++) {
                if (bookcases[t] != null) {

                    for (int k = 0; k < bookcases[t].getLengthBookcase(); k++) {

                        if (bookcases[t].getShelf(k) != null) {
                            for (int i = 0; i < bookcases[t].getShelf(k).getLengthShell(); i++) {  // przegladanie polki

                                if (bookcases[t].getShelf(k).getBook(i) != null) {
                                    bw.write(i + " " + k + " " + t);
                                    bw.newLine();
                                    bw.write(bookcases[t].getShelf(k).getBook(i).getTitle());
                                    bw.newLine();
                                    bw.write(bookcases[t].getShelf(k).getBook(i).getAuthor());
                                    bw.newLine();
                                    bw.write(bookcases[t].getShelf(k).getBook(i).getYear());
                                    bw.newLine();
                                }
                            }
                        }
                    }
                }
            }

        } catch(IOException ex){
            System.err.println(ex.getMessage());
            ex.printStackTrace();
            }
        }

        public void writeToBinary(String path){
            try (FileOutputStream fos = new FileOutputStream(path);
                 ObjectOutputStream oos = new ObjectOutputStream(fos);) {

                oos.writeObject(bookcases);

            } catch (IOException ex) {
                System.err.println(ex.getMessage());
                ex.printStackTrace();
            }
        }








//    public int getLenghtLibrary(){
//        return bookcases.length;
//    }
}
