package Library;

import java.io.Serializable;

/**
 * Created by maryb on 01.06.2017.
 */
public class Shell implements Serializable {

    // W ramach metodymainproszę zastąpić wykorzystanie tablicyksiążek na wykorzystanie obiektu półki


    private Book [] books = new Book[10];


    public void putBook(int index, Book book) {
        if (index > 0 && index < books.length) {
            books[index] = book;
        }
    }

    public Book getBook(int index) {
        if (index > 0 && index < books.length){
            //System.out.println("Oto Twoja ksiazka");
            return books[index];
        }
        else{
            //System.out.println("Nie ma czego zwracać");
            return null;
        }
    }

    public int getLengthShell(){
        return books.length;
    }

}

